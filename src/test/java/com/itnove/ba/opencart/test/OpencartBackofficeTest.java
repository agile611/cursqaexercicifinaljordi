package com.itnove.ba.opencart.test;

import com.itnove.ba.opencart.BaseTestOCboadmin;
import com.itnove.ba.opencart.pages.BackofficeAdmin;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OpencartBackofficeTest extends BaseTestOCboadmin {

    /*
     * Test 1: Login correcte a backoffice d'Opencart
     */
    @Test
    public void LoginBackoffice() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        BackofficeAdmin admin = new BackofficeAdmin(driver);

        Assert.assertTrue(admin.loginPageDisplayed(wait));
        admin.loginAdmin("user","bitnami1");

        Assert.assertTrue(admin.htmlBodyLoaded(wait));

        if (admin.modalSecurityAlert.isDisplayed())
            admin.closeModalSecurityAlert.click();

        Assert.assertTrue(admin.backofficeLoginSuccesful(wait));
    }

    /*
     * Test 2: Logout a backoffice d'Opencart
     */
    @Test (dependsOnMethods={"LoginBackoffice"})
    public void LogoutBackoffice() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        BackofficeAdmin admin = new BackofficeAdmin(driver);

        Assert.assertTrue(admin.backofficeLoginSuccesful(wait));
        admin.logOutBackoffice.click();

        Assert.assertTrue(admin.loginPageDisplayed(wait));
    }

    /*
     * Test 3: Login password incorrecte a backoffice d'Opencart
     */
    @Test (dependsOnMethods={"LogoutBackoffice"})
    public void LoginWrongPassword() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        BackofficeAdmin admin = new BackofficeAdmin(driver);

        Assert.assertTrue(admin.loginPageDisplayed(wait));
        admin.loginAdmin("user","1imantib");

        Assert.assertTrue(admin.loginAlertMessageDisplayed(wait));
        Assert.assertTrue(admin.loginAlertMessage().contains("match"));
    }

    /*
     * Test 4: Login password incorrecte a backoffice d'Opencart
     */
    @Test (dependsOnMethods={"LoginWrongPassword"})
    public void LoginWrongUsername() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        BackofficeAdmin admin = new BackofficeAdmin(driver);

        Assert.assertTrue(admin.loginPageDisplayed(wait));
        admin.loginAdmin("fail","bitnami1");

        Assert.assertTrue(admin.loginAlertMessageDisplayed(wait));
        Assert.assertTrue(admin.loginAlertMessage().contains("match"));
    }

    /*
     * Test 5: Login password i username incorrecte a backoffice d'Opencart
     */
    @Test (dependsOnMethods={"LoginWrongUsername"})
    public void LoginWrongUserAndPwd() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        BackofficeAdmin admin = new BackofficeAdmin(driver);

        Assert.assertTrue(admin.loginPageDisplayed(wait));
        admin.loginAdmin("fail","1imantib");

        Assert.assertTrue(admin.loginAlertMessageDisplayed(wait));
        Assert.assertTrue(admin.loginAlertMessage().contains("match"));
    }

    /*
     * Test 6: Login password buit a backoffice d'Opencart
     */
    @Test (dependsOnMethods={"LoginWrongUserAndPwd"})
    public void LoginEmptyPassword() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        BackofficeAdmin admin = new BackofficeAdmin(driver);

        Assert.assertTrue(admin.loginPageDisplayed(wait));
        admin.loginAdmin("user","");

        Assert.assertTrue(admin.loginAlertMessageDisplayed(wait));
        Assert.assertTrue(admin.loginAlertMessage().contains("match"));
    }

    /*
     * Test 7: Login username buit a backoffice d'Opencart
     */
    @Test (dependsOnMethods={"LoginEmptyPassword"})
    public void LoginEmptyUsername() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        BackofficeAdmin admin = new BackofficeAdmin(driver);

        Assert.assertTrue(admin.loginPageDisplayed(wait));
        admin.loginAdmin("","bitnami1");

        Assert.assertTrue(admin.loginAlertMessageDisplayed(wait));
        Assert.assertTrue(admin.loginAlertMessage().contains("match"));
    }

    /*
     * Test 8: Login camps buits a backoffice d'Opencart
     */
    @Test (dependsOnMethods={"LoginEmptyUsername"})
    public void LoginEmptyFields() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        BackofficeAdmin admin = new BackofficeAdmin(driver);

        Assert.assertTrue(admin.loginPageDisplayed(wait));
        admin.loginAdmin("","");

        Assert.assertTrue(admin.loginAlertMessageDisplayed(wait));
        Assert.assertTrue(admin.loginAlertMessage().contains("match"));
    }
}