package com.itnove.ba.opencart.test;

import com.itnove.ba.opencart.BaseTestOC;
import com.itnove.ba.opencart.pages.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OpencartElementsTest extends BaseTestOC {

    /*
     * Test 1: Pàgina web e-commerce 'Your Store' carregada
     */
    @Test
    public void HomePageLoaded() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        LoadHome load = new LoadHome(driver);

        Assert.assertTrue(load.pageDisplayed(wait));
        Assert.assertEquals("Your Store", driver.getTitle());
        System.out.println("TC1.- Pàgina 'Your Store' carregada");
    }

    /*
     * Test 2: Comprovar que es genera una llista en fer click a un menu
     */
    @Test (dependsOnMethods={"HomePageLoaded"})
    public void ExistingItemsList() throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        MenuClick menu = new MenuClick(driver);
        ItemSearch search = new ItemSearch(driver);

        menu.componentsDropdown.click();
        menu.monitorsLink.click();

        Assert.assertTrue(search.listedElementExists(wait));
        System.out.println("TC2.- Llista d'elements cercats existent");
    }

    /*
     * Test 3: Comprovar que no es genera una llista en fer click a un menu amb items = 0
     */
    @Test (dependsOnMethods={"ExistingItemsList"})
    public void NonExistingItemList() throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        MenuClick menu = new MenuClick(driver);
        ItemSearch search = new ItemSearch(driver);

        menu.laptopsDropdown.click();
        menu.windowsLink.click();

        Assert.assertFalse(search.listedElementExists(wait));
        System.out.println("TC3.- Llista d'elements cercats no existent");
    }

    /*
     * Test 4: Carregar pàgina de registre
     */
    @Test (dependsOnMethods={"NonExistingItemList"})
    public void LoadRegisterForm() throws InterruptedException {
        Actions actionsHover = new Actions(driver);
        UserAccount account = new UserAccount(driver);

        // 1
        account.myAccountDropdown.click();
        Thread.sleep(500);

        // 2
        account.hoverRegister(actionsHover);
        account.registerLink.click();

        // 3
        Assert.assertTrue(account.registerFormLoaded(wait));
        System.out.println("TC4.- Pagina de registre carregada");

    }

    /*
     * Test 5: Comprovar que s'activa actionHover en passar per sobre del menu
     */
    @Test (dependsOnMethods={"NonExistingItemList"})
    public void MenuHoverAndClick() throws InterruptedException {
        MenuHover menu = new MenuHover(driver);
        menu.desktopHoverActions(driver, actionHover);

        System.out.println("TC5.- Es mostren i s'activen els elements en fer mouseover");
    }

}
