package com.itnove.ba.opencart.test;

import com.itnove.ba.opencart.BaseTestOCregister;
import com.itnove.ba.opencart.pages.RegisterUser;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class OpencartRegisterTest extends BaseTestOCregister {

    /*
     * Test 1: Comprovar que es registra satisfactoriament un nou usuari
     */
    @Test
    public void registerUserSuccess() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        RegisterUser register = new RegisterUser(driver);

        Assert.assertTrue(register.pageDisplayed(wait));
        UUID uuid = UUID.randomUUID();
        String email = uuid+"@bcnactiva.cat";

        register.fillRegisterForm(
                "Cibernarium",
                "BcnActiva",
                uuid+"@bcnactiva.cat",
                "+34-666-666-666",
                "contrasenya",
                "contrasenya");

        register.setAgreeConditions();
        register.submitForm.click();

        Assert.assertTrue(register.signUpMessageDisplayed(wait));
        Assert.assertTrue(register.successfulRegisterMessage(wait));
        System.out.println("TC1.- Registre d'usuari creat, OK");

        register.removeRegisteredUser(uuid+"@bcnactiva.cat", driver, wait);
        driver.navigate().to("http://opencart.votarem.lu/index.php?route=account/register");
    }

    /*
     * Test 2: Comprovar que es mostren errors de camps buits en el proces de registre
     */
    @Test (dependsOnMethods={"registerUserSuccess"})
    public void registerEmptyUserFields() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        RegisterUser register = new RegisterUser(driver);

        Assert.assertTrue(register.pageDisplayed(wait));

        register.fillRegisterForm(
                "",
                "",
                "",
                "",
                "contrasenya",
                "contrasenya");

        register.setAgreeConditions();
        register.submitForm.click();

        Assert.assertTrue(register.firstNameError());
        Assert.assertTrue(register.lastNameError());
        Assert.assertTrue(register.emailError());
        Assert.assertTrue(register.phoneError());

        System.out.println("TC2.- Registre d'usuari error camps buits");
    }

    /*
     * Test 3: Comprovar que es mostra error de contrasenya buida en el proces de registre
     */
    @Test (dependsOnMethods={"registerUserSuccess"})
    public void registerEmptyPassword() throws InterruptedException {
        RegisterUser register = new RegisterUser(driver);
        UUID uuid = UUID.randomUUID();

        register.fillRegisterForm(
                "Cibernarium",
                "BcnActiva",
                uuid+"@barcelonactiva.cat",
                "+34-666-666-666",
                "",
                "");

        register.setAgreeConditions();
        register.submitForm.click();

        Assert.assertTrue(register.passwordError());

        System.out.println("TC3.- Registre d'usuari error contrasenya buida");
    }

    /*
     * Test 4: Comprovar que es mostra error de contrasenya curta en el proces de registre
     */
    @Test (dependsOnMethods={"registerUserSuccess"})
    public void registerShortPassword() throws InterruptedException {
        RegisterUser register = new RegisterUser(driver);
        UUID uuid = UUID.randomUUID();

        register.fillRegisterForm(
                "Cibernarium",
                "BcnActiva",
                uuid+"@barcelonactiva.cat",
                "+34-666-666-666",
                "con",
                "con");

        register.setAgreeConditions();
        register.submitForm.click();

        Assert.assertTrue(register.passwordError());

        System.out.println("TC4.- Registre d'usuari error contrasenya curta");
    }

    /*
     * Test 5: Comprovar que es mostra error de contrasenya curta en el proces de registre
     */
    @Test (dependsOnMethods={"registerUserSuccess"})
    public void registerEmptyConfirm() throws InterruptedException {
        RegisterUser register = new RegisterUser(driver);
        UUID uuid = UUID.randomUUID();

        register.fillRegisterForm(
                "Cibernarium",
                "BcnActiva",
                uuid+"@barcelonactiva.cat",
                "+34-666-666-666",
                "contrasenya",
                "");

        register.setAgreeConditions();
        register.submitForm.click();

        Assert.assertTrue(register.confirmError());

        System.out.println("TC5.- Registre d'usuari error confirmacio i contrasenya buida");
    }

    /*
     * Test 6: Comprovar que es mostra error de contrasenya curta i error de confirmacio
     *         de contrasenya en el proces de registre
     */
    @Test (dependsOnMethods={"registerUserSuccess"})
    public void registerShortPwdErrorConfirm() throws InterruptedException {
        RegisterUser register = new RegisterUser(driver);
        UUID uuid = UUID.randomUUID();

        register.fillRegisterForm(
                "Cibernarium",
                "BcnActiva",
                uuid+"@barcelonactiva.cat",
                "+34-666-666-666",
                "con",
                "noc");

        register.setAgreeConditions();
        register.submitForm.click();

        Assert.assertTrue(register.passwordError());
        Assert.assertTrue(register.confirmError());

        System.out.println("TC6.- Registre d'usuari contrasenya curta i error confirmacio");
    }

    /*
     * Test 7: Comprovar que es mostra error de no acceptacio de condicions
     */
    @Test (dependsOnMethods={"registerUserSuccess"})
    public void registerNoAcceptedConditions() throws InterruptedException {
        RegisterUser register = new RegisterUser(driver);
        UUID uuid = UUID.randomUUID();

        register.fillRegisterForm(
                "Cibernarium",
                "BcnActiva",
                uuid+"@barcelonactiva.cat",
                "+34-666-666-666",
                "contrasenya",
                "contrasenya");

        register.setNoAgreeConditions();
        register.submitForm.click();

        Assert.assertTrue(register.acceptConditionsError());

        System.out.println("TC7.- Registre d'usuari error condicions no acceptades");
    }
}