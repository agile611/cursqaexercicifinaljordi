package com.itnove.ba.opencart.test;

import com.itnove.ba.opencart.BaseTestOC;
import com.itnove.ba.opencart.pages.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OpencartSearchTest extends BaseTestOC {

    /*
     * Test 1: Pàgina web e-commerce 'Your Store' carregada
     */
    @Test
    public void HomePageLoaded() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        LoadHome load = new LoadHome(driver);

        Assert.assertTrue(load.pageDisplayed(wait));
        Assert.assertEquals("Your Store", driver.getTitle());
        System.out.println("TC1.- Pàgina 'Your Store' carregada");
    }

    /*
     * Test 2: Comprovar que genera una llista en fer cerca de producte existent
     */
    @Test (dependsOnMethods={"HomePageLoaded"})
    public void SearchFoundItemsList() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        LoadHome load = new LoadHome(driver);
        ItemSearch search = new ItemSearch(driver);

        Assert.assertTrue(load.pageDisplayed(wait));

        search.submitSeaarch("Samsung");
        Assert.assertTrue(search.searchedElementExists(wait));
        System.out.println("TC2.- Llista d'elements cercats existent");
    }

    /*
     * Test 3: Comprovar que no es genera una llista en fer cerca de producte no existent
     */
    @Test (dependsOnMethods={"SearchFoundItemsList"})
    public void SearchNotFoundList() throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        ItemSearch search = new ItemSearch(driver);

        search.submitSeaarch("mobile");
        Assert.assertFalse(search.searchedElementExists(wait));
        System.out.println("TC3.- Element de cerca no existent");
    }
}