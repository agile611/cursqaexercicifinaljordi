package com.itnove.ba.opencart.test;

import com.itnove.ba.opencart.BaseTestOC;
import com.itnove.ba.opencart.pages.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OpencartCheckoutTest extends BaseTestOC {

    /*
     * Test 1: Pàgina web e-commerce 'Your Store' carregada
     */
    @Test
    public void HomePageLoaded() throws InterruptedException {
        LoadHome load = new LoadHome(driver);

        Assert.assertTrue(load.pageDisplayed(wait));
        Assert.assertEquals("Your Store", driver.getTitle());
        System.out.println("TC1.- Pàgina 'Your Store' carregada");
    }

    /*
     * Test 2: Comprovar que s'afegeix un element a la cistella
     */
    @Test (dependsOnMethods={"HomePageLoaded"})
    public void ItemAddedToCart() throws InterruptedException {
        LoadHome load = new LoadHome(driver);
        Actions actionHover = new Actions(driver);
        AddToCart cart = new AddToCart(driver);

        Assert.assertTrue(load.pageDisplayed(wait));
        Assert.assertEquals("Your Store", driver.getTitle());

        cart.hoverDesktopDropdown(actionHover);
        cart.macLink.click();
        cart.addToCartButton.click();
        cart.cartDropdown.click();
        cart.cartOption.click();
        Assert.assertTrue(cart.registerFormLoaded(wait));
        Assert.assertEquals("$122.00",cart.amountInformation.getText());
        System.out.println("TC2.- Element iMac afegit al cistell amb preu $122.00");
    }

    /*
     * Test 3: Procés de compra
     */
    @Test (dependsOnMethods={"ItemAddedToCart"})
    public void CheckoutProcess() throws InterruptedException {
        LoadHome load = new LoadHome(driver);
        Checkout checkout = new Checkout(driver);

        Assert.assertTrue(load.pageDisplayed(wait));
        Assert.assertEquals("Your Store", driver.getTitle());

        checkout.clickCheckoutLink();
        checkout.checkoutLoaded(wait);

        checkout.guestRadioButton.click();
        checkout.continueButton.click();

        checkout.guestButtonLoaded(wait);
        String[] personalDetails = {
                "Cibernarium",
                "BarcelonActiva",
                "unnamed.user@example.com",
                "34-666-666-666",
                "Carrer de Roc Boronat, 117",
                "Barcelona",
                "08019",
                "Spain",
                "Barcelona"
        };
        checkout.shippingData(personalDetails);
        checkout.continueGuestButton.click();

        wait.until(ExpectedConditions.visibilityOf(checkout.continueShippingMethodButton));
        checkout.continueShippingMethodButton.click();

        wait.until(ExpectedConditions.visibilityOf(checkout.agreeLegalConditionsCheckbox));
        checkout.agreeLegalConditionsCheckbox.click();
        checkout.continuePaymentMethodButton.click();

        wait.until(ExpectedConditions.visibilityOf(checkout.confirmOrderButton));
        checkout.confirmOrderButton.click();

        Assert.assertTrue(checkout.orderConfirmedMessageLoaded(wait));
        Assert.assertEquals(checkout.getConfirmMessage(),"Your order has been placed!");
        System.out.println("TC3.- Proces de compra satisfactori: "+checkout.getConfirmMessage());
    }
}
