package com.itnove.ba.opencart;

import com.itnove.ba.BaseTest;
import org.testng.annotations.BeforeClass;

public class BaseTestOCboadmin extends BaseTest{

    @BeforeClass
    public void loadURL() {
        int width = driver.manage().window().getSize().getWidth();
        if (width < 1200)
            display = "mobile";

        driver.navigate().to("http://opencart.votarem.lu/admin");
    }
}
