package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by jPeralta on 09-12-2017.
 */
public class ItemSearch {

    @FindBy(xpath = "//*[@id='search']/input")
    public WebElement searchInput;

    @FindBy(xpath = "//*[@id='search']/span/button")
    public WebElement searchButton;

    @FindBy(xpath = "//*[@id='product-search']/ul/li[2]/a")
    public WebElement searchBreadcrumbs;

    @FindBy(xpath = "//*[@id='product-category']/ul/li[3]/a")
    public WebElement productCategoryBreadcrumbs;

    @FindBy(className = "product-layout")
    public List<WebElement> listedElements;

    public void submitSeaarch(String key) {
        searchInput.sendKeys(key);
        searchButton.click();
    }

    public boolean searchedElementExists(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(searchBreadcrumbs));
        return listedElements.size() > 0;
    }

    public boolean listedElementExists(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(productCategoryBreadcrumbs));
        return listedElements.size() > 0;
    }

    public ItemSearch(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
