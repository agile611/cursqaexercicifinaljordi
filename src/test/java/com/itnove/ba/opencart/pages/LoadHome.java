package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by jPeralta on 09-12-2017.
 */
public class LoadHome {

    @FindBy(xpath = "//*[@id='logo']/h1/a")
    public WebElement titleId;

    @FindBy(xpath = "//*[@id='top-links']//a[@title='My Account']/i")
    public WebElement myAccount;

    @FindBy(xpath = "//*[@id='top-links']//a[@title='My Account']/../ul/li[1]")
    public WebElement registerUser;

    public boolean pageDisplayed(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(titleId));
        return titleId.isDisplayed();
    }

    public LoadHome(WebDriver driver) { PageFactory.initElements(driver, this); }
}
