package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Created by jPeralta on 09-12-2017.
 */
public class RegisterUser {

    @FindBy(xpath = "//*[@id='logo']/h1/a")
    public WebElement titleId;

    @FindBy(xpath = "//input[@id='input-firstname']")
    public WebElement firstNameInputField;

    @FindBy(xpath = "//input[@id='input-firstname']/../div")
    public WebElement firstNameErrorAlert;

    @FindBy(xpath = "//input[@id='input-lastname']")
    public WebElement lastNameInputField;

    @FindBy(xpath = "//input[@id='input-lastname']/../div")
    public WebElement lastNameErrorAlert;

    @FindBy(xpath = "//input[@id='input-email']")
    public WebElement emailInputField;

    @FindBy(xpath = "//input[@id='input-email']/../div")
    public WebElement emailErrorAlert;

    @FindBy(xpath = "//input[@id='input-telephone']")
    public WebElement phoneInputField;

    @FindBy(xpath = "//input[@id='input-telephone']/../div")
    public WebElement phoneErrorAlert;

    @FindBy(xpath = "//input[@id='input-password']")
    public WebElement passwordInputField;

    @FindBy(xpath = "//input[@id='input-password']/../div")
    public WebElement passwordErrorAlert;

    @FindBy(xpath = "//input[@id='input-confirm']")
    public WebElement confirmInputField;

    @FindBy(xpath = "//input[@id='input-confirm']/../div")
    public WebElement confirmErrorAlert;

    @FindBy(xpath = "//*[@id='content']/form//input[@name='agree']")
    public WebElement agreeConditions;

    @FindBy(xpath = "//*[@id='content']/form//input[@type='submit'][@value='Continue']")
    public WebElement submitForm;

    @FindBy(xpath = "//*[@id='content']")
    public WebElement registerMessage;

    @FindBy(xpath = "//*[@id='content']/h1")
    public WebElement registeredAccountTitle;

    @FindBy(xpath = "//*[@id='account-register']/div[@class='alert alert-danger alert-dismissible']")
    public WebElement alertDanger;

    public boolean pageDisplayed(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(titleId));
        return titleId.isDisplayed();
    }

    public void fillRegisterForm(
            String firstName,
            String lastName,
            String email,
            String phone,
            String pwd,
            String confirm) {
        firstNameInputField.clear();
        firstNameInputField.sendKeys(firstName);
        lastNameInputField.clear();
        lastNameInputField.sendKeys(lastName);
        emailInputField.clear();
        emailInputField.sendKeys(email);
        phoneInputField.clear();
        phoneInputField.sendKeys(phone);
        passwordInputField.clear();
        passwordInputField.sendKeys(pwd);
        confirmInputField.clear();
        confirmInputField.sendKeys(confirm);
    }

    public boolean signUpMessageDisplayed(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(registeredAccountTitle));
        //  http://opencart.votarem.lu/index.php?route=account/success
        return registeredAccountTitle.isDisplayed();
    }

    public boolean successfulRegisterMessage(WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(registerMessage));
        System.out.println(registerMessage.getText());
        return registerMessage.getText().toLowerCase().contains("congratulations");
    }

    public boolean firstNameError() {
        return firstNameErrorAlert.
            getText().equalsIgnoreCase(
            "First Name must be between 1 and 32 characters!");
    }

    public boolean lastNameError() {
        return lastNameErrorAlert.
            getText().equalsIgnoreCase(
            "Last Name must be between 1 and 32 characters!");
    }

    public boolean emailError() {
        return emailErrorAlert.
            getText().equalsIgnoreCase(
            "E-Mail Address does not appear to be valid!");
    }

    public boolean phoneError() {
        return phoneErrorAlert.
                getText().equalsIgnoreCase(
                "Telephone must be between 3 and 32 characters!");
    }

    public boolean passwordError() {
        return passwordErrorAlert.
                getText().equalsIgnoreCase(
                "Password must be between 4 and 20 characters!");
    }

    public boolean confirmError() {
        return confirmErrorAlert.
                getText().equalsIgnoreCase(
                "Password confirmation does not match password!");
    }

    public boolean acceptConditionsError() {
        return alertDanger.getText().contains("Privacy");
    }

    public void removeRegisteredUser(String email, WebDriver driver, WebDriverWait wait) throws InterruptedException {
        BackofficeAdmin admin = new BackofficeAdmin(driver);

        driver.navigate().to("http://opencart.votarem.lu/admin/");
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOf(admin.loginButton));

        admin.loginAdmin("user", "bitnami1");

        wait.until(ExpectedConditions.visibilityOf(admin.closeModalSecurityAlert));
        admin.closeModalSecurityAlert.click();
        admin.customerDropdown.click();
        wait.until(ExpectedConditions.visibilityOf(admin.customerLink));
        admin.customerLink.click();

        wait.until(ExpectedConditions.visibilityOf(admin.filterCustomerByEmailInput));
        admin.searchEmail(email);

        Assert.assertTrue(admin.userByEmailIsRegistered(email, wait));
        admin.removeFirstListedUser();

        driver.switchTo().alert().accept();

        wait.until(ExpectedConditions.visibilityOf(admin.deletedUserConfirmation));
        Assert.assertTrue(admin.deletedUserConfirmation.getText().contains("Success"));
    }

    public void setAgreeConditions() {
        if (!agreeConditions.isSelected())
            agreeConditions.click();
    }

    public void setNoAgreeConditions() {
        if (agreeConditions.isSelected())
            agreeConditions.click();
    }

    public RegisterUser(WebDriver driver) { PageFactory.initElements(driver, this); }
}
