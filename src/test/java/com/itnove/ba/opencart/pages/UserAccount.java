package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

import static sun.audio.AudioDevice.device;

/**
 * Created by jPeralta on 09-12-2017.
 */
public class UserAccount {

    @FindBy(xpath = "//*[@id='top-links']/ul/li[2]/a")
    public WebElement myAccountDropdown;

    @FindBy(xpath = "//*[@id='top-links']/ul/li[2]/ul/li[1]/a")
    public WebElement registerLink;

    @FindBy(id = "account-register")
    public WebElement registerTagId;

    public void hoverRegister (Actions hover) {
        if (device.equals("desktop"))
            hover.moveToElement(registerLink).build().perform();
        else
            registerLink.click();
    }

    public boolean registerFormLoaded(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(registerTagId));
        return registerTagId.isDisplayed();
    }

    public UserAccount(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
