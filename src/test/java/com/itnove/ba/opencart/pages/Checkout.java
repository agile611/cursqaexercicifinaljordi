package com.itnove.ba.opencart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.itnove.ba.BaseTest.display;

/**
 * Created by jPeralta on 10-12-2017.
 */
public class Checkout {

    @FindBy(xpath = "//*[@id='top-links']/ul/li[5]/a[@title='Checkout']/span")
    public WebElement checkoutDesktopLink;

    @FindBy(xpath = "//*[@id='top-links']/ul/li[5]/a[@title='Checkout']/i")
    public WebElement checkoutMobileLink;

    @FindBy(xpath = "//*[@id='checkout-checkout']/ul/li[3]/a")
    public WebElement checkoutBreadcrumb;

    @FindBy(xpath = "//*[@id='collapse-checkout-option']/div/div/div[1]/div/label/input[@value='guest']")
    public WebElement guestRadioButton;

    @FindBy(xpath = "//*[@id='button-account']")
    public WebElement continueButton;

    @FindBy(xpath = "//*[@id='button-guest']")
    public WebElement continueGuestButton;

    @FindBy(xpath = "//*[@id='button-shipping-method']")
    public WebElement continueShippingMethodButton;

    @FindBy(xpath = "//*[@id='input-payment-firstname']")
    public WebElement inputPaymentFirstname;

    @FindBy(xpath = "//*[@id='input-payment-lastname']")
    public WebElement inputPaymentLastname;

    @FindBy(xpath = "//*[@id='input-payment-email']")
    public WebElement inputPaymentEmail;

    @FindBy(xpath = "//*[@id='input-payment-telephone']")
    public WebElement inputPaymentTelephone;

    @FindBy(xpath = "//*[@id='input-payment-address-1']")
    public WebElement inputPaymentAddress;

    @FindBy(xpath = "//*[@id='input-payment-city']")
    public WebElement inputPaymentCity;

    @FindBy(xpath = "//*[@id='input-payment-postcode']")
    public WebElement inputPaymentPostcode;

    @FindBy(xpath = "//*[@id='input-payment-country']")
    public WebElement inputPaymentCountry;

    @FindBy(xpath = "//*[@id='input-payment-zone']")
    public WebElement inputPaymentZone;

    @FindBy(xpath = "//*[@id='collapse-payment-method']/div/div[2]/div/input[@name='agree']")
    public WebElement agreeLegalConditionsCheckbox;

    @FindBy(xpath = "//*[@id='button-payment-method']")
    public WebElement continuePaymentMethodButton;

    @FindBy(xpath = "//*[@id='button-confirm']")
    public WebElement confirmOrderButton;

    @FindBy(xpath = "//*[@id='content']/h1")
    public WebElement confirmMessage;

    @FindBy(xpath = "//*[@id='common-success']/ul[@class='breadcrumb']/li[4]/a")
    public WebElement orderConfirmedBreadcrumb;


    public void clickCheckoutLink () {
        if (display.equalsIgnoreCase("desktop"))
            checkoutDesktopLink.click();
        else
            checkoutMobileLink.click();
    }

    public boolean checkoutLoaded(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(checkoutBreadcrumb));
        return checkoutBreadcrumb.isDisplayed();
    }

    public boolean guestButtonLoaded(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(continueGuestButton));
        return continueGuestButton.isDisplayed();
    }

    public void shippingData(String[] data) {
        inputPaymentFirstname.clear();
        inputPaymentFirstname.sendKeys(data[0]);

        inputPaymentLastname.clear();
        inputPaymentLastname.sendKeys(data[1]);

        inputPaymentEmail.clear();
        inputPaymentEmail.sendKeys(data[2]);

        inputPaymentTelephone.clear();
        inputPaymentTelephone.sendKeys(data[3]);

        inputPaymentAddress.clear();
        inputPaymentAddress.sendKeys(data[4]);

        inputPaymentCity.clear();
        inputPaymentCity.sendKeys(data[5]);

        inputPaymentPostcode.clear();
        inputPaymentPostcode.sendKeys(data[6]);

        Select selectPaymentCountry = new Select(inputPaymentCountry);
        selectPaymentCountry.selectByVisibleText(data[7]);

        Select selectPaymentZone = new Select(inputPaymentZone);
        selectPaymentZone.selectByVisibleText(data[8]);
    }

    public boolean orderConfirmedMessageLoaded(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(orderConfirmedBreadcrumb));
        return orderConfirmedBreadcrumb.isDisplayed();
    }

    public String getConfirmMessage() {
        return confirmMessage.getText();
    }

    public Checkout(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
