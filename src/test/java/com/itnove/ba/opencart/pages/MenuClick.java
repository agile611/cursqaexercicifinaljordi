package com.itnove.ba.opencart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

/**
 * Created by jPeralta on 09-12-2017.
 */
public class MenuClick {

    @FindBy(xpath = "//*[@id='menu']/div[2]/ul/li[3]/a")
    public WebElement componentsDropdown;

    @FindBy(xpath = "//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[2]/a")
    public WebElement monitorsLink;

    @FindBy(xpath = "//*[@id='menu']/div[2]/ul/li[2]/a")
    public WebElement laptopsDropdown;

    @FindBy(xpath = "//*[@id='menu']/div[2]/ul/li[2]/div/div/ul/li[1]/a")
    public WebElement windowsLink;

    public MenuClick(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
