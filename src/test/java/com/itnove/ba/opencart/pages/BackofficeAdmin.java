package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by jperalta on 11-12-2017.
 */
public class BackofficeAdmin {

    @FindBy(xpath = "//form[@action='http://opencart.votarem.lu/admin/index.php?route=common/login']//button[@type='submit']")
    public WebElement loginButton;

    @FindBy(id = "input-username")
    public WebElement userNameInput;

    @FindBy(id = "input-password")
    public WebElement passwordInput;

    @FindBy(xpath = "//*[@id='content']//div[@class='alert alert-danger alert-dismissible']")
    public WebElement loginAlertDanger;

    @FindBy(xpath = "//*[@id='header']/div/ul/li[@class='dropdown']")
    public WebElement loggedUserInBackoffice;

    @FindBy(xpath = "//*[@id='header']/div/ul/li[2]")
    public WebElement logOutBackoffice;

    @FindBy(xpath = "/html/body")
    public WebElement htmlBodyTag;

    @FindBy(xpath = "//*[@id='modal-security']//div[@class='modal-content']")
    public WebElement modalSecurityAlert;

    @FindBy(xpath = "//*[@id='modal-security']//button[@class='close'][@data-dismiss='modal']")
    public WebElement closeModalSecurityAlert;

    @FindBy(id = "menu-customer")
    public WebElement customerDropdown;

    @FindBy(xpath = "//ul[@id='collapse33']/li[1]")
    public WebElement customerLink;

    @FindBy(id = "input-email")
    public WebElement filterCustomerByEmailInput;

    @FindBy(id = "button-filter")
    public WebElement filterSubmit;

    @FindBy(xpath = "//*[@id='form-customer']/table/tbody/tr[1]/td[3]")
    public WebElement firstCustomerEmailListed;

    @FindBy(xpath = "//*[@id='form-customer']/table/tbody/tr/td[1]/input")
    public WebElement firstCustomerCheckBox;

    @FindBy(xpath = "//*[@id='content']/div[1]/div/div/button[@data-original-title='Delete']")
    public WebElement deleteElement;

    @FindBy(xpath = "//*[@id='content']/div[2]/div[1]")
    public WebElement deletedUserConfirmation;


    public void loginAdmin (String username, String password) {

        userNameInput.clear();
        userNameInput.sendKeys(username);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        loginButton.click();
    }

    public void searchEmail(String email) {

        filterCustomerByEmailInput.clear();
        filterCustomerByEmailInput.sendKeys(email);
        filterSubmit.click();
    }

    public boolean userByEmailIsRegistered(String email, WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(firstCustomerEmailListed));
        return firstCustomerEmailListed.getText().equalsIgnoreCase(email);
    }

    public void removeFirstListedUser() {
        firstCustomerCheckBox.click();
        deleteElement.click();
    }

    public boolean loginAlertMessageDisplayed(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(loginAlertDanger));
        return loginAlertDanger.isDisplayed();
    }

    public String loginAlertMessage() {
        return loginAlertDanger.getText();
    }

    public boolean loginPageDisplayed(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(loginButton));
        return loginButton.isDisplayed();
    }

    public boolean htmlBodyLoaded(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(htmlBodyTag));
        return htmlBodyTag.isDisplayed();
    }

    public boolean backofficeLoginSuccesful(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(loggedUserInBackoffice));
        return loggedUserInBackoffice.isDisplayed();
    }

    public BackofficeAdmin(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
