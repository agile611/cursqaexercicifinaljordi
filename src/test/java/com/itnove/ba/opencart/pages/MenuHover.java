package com.itnove.ba.opencart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

/**
 * Created by jPeralta on 09-12-2017.
 */
public class MenuHover {

    @FindBy(xpath = ".//*[@id='menu']/div[2]/ul/li")
    public List<WebElement> elementsMenuDesktop;

    @FindBy(xpath = "//*[@id='product-category']/ul")
    public WebElement breadcrumb;

    public void desktopHoverActions(WebDriver driver, Actions actionHover) throws InterruptedException {
        int elements = elementsMenuDesktop.size();
        System.out.println(elements);

        for (int i = 1; i < elements + 1; i++){

            WebElement element = driver.findElement(By.xpath(".//*[@id='menu']/div[2]/ul/li["+i+"]/a"));
            actionHover.moveToElement(element).build().perform();
            System.out.print("Menu "+i+" hovered. ");
            Thread.sleep(500);
            int showAllElements = driver.findElements(By.xpath(".//*[@id='menu']/div[2]/ul/li["+i+"]/div/a")).size();
            int subMenuColumns = driver.findElements(By.xpath("//*[@id='menu']/div[2]/ul/li["+i+"]/div/div/ul")).size();

            for (int j = 1; j < subMenuColumns + 1; j++){
                int subElements = driver.findElements(By.xpath(".//*[@id='menu']/div[2]/ul/li["+i+"]/div/div/ul["+j+"]/li")).size();
                for (int k = 1; k < subElements + 1; k++){
                    WebElement subElement = driver.findElement(By.xpath(".//*[@id='menu']/div[2]/ul/li["+i+"]/div/div/ul["+j+"]/li["+k+"]"));
                    actionHover.moveToElement(subElement).build().perform();
                    Thread.sleep(250);
                }
            }

            if (showAllElements > 0) {
                WebElement selectAll = driver.findElement(By.xpath("//*[@id='menu']/div[2]/ul/li["+i+"]/div/a"));
                actionHover.moveToElement(selectAll).build().perform();
                Thread.sleep(500);
                Assert.assertTrue(selectAll.isDisplayed());
                System.out.print("Submenu "+i+" displayed. ");
                selectAll.click();
                WebDriverWait wait = new WebDriverWait(driver,10);
                wait.until(ExpectedConditions.visibilityOf(breadcrumb));
                Assert.assertTrue(this.breadcrumb.isDisplayed());
                System.out.print("Elements of menu "+i+" hovered in new page. ");
            }
            System.out.println("");

        }
    }

    public MenuHover(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
