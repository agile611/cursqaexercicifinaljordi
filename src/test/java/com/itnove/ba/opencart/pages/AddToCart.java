package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static com.itnove.ba.BaseTest.display;

/**
 * Created by jPeralta on 10-12-2017.
 */
public class AddToCart {

    @FindBy(xpath = "//*[@id='menu']/div[2]/ul/li[1]/a")
    public WebElement desktopDropdown;

    @FindBy(xpath = "//nav[@id='menu']/div[1]/button")
    public WebElement mobileCategoriesDropdown;

    @FindBy(xpath = "//*[@id='menu']/div[2]/ul/li[1]/div/div/ul/li[2]/a")
    public WebElement macLink;

    @FindBy(xpath = "//*[@id='content']/div[2]/div/div/div[1]/a")
    public WebElement addToCartButton;

    @FindBy(xpath = "//*[@id='button-cart']")
    public WebElement cartDropdown;

    @FindBy(xpath = "//*[@id='cart']/button")
    public WebElement cartOption;

    @FindBy(xpath = "//*[@id='cart']/ul/li[2]/div/table/tbody/tr[4]/td[2]")
    public WebElement amountInformation;

    public void hoverDesktopDropdown (Actions hover) {
        if (display.equals("desktop")) {
            hover.moveToElement(desktopDropdown).build().perform();
        } else {
            mobileCategoriesDropdown.click();
            desktopDropdown.click();
        }
    }

    public boolean registerFormLoaded(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(amountInformation));
        return amountInformation.isDisplayed();
    }

    public AddToCart(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
