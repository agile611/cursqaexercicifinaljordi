Test Plan e-commerce OPENCART
===
---
Referències
---

Nom del Document: **TestPlan Opencart.BcnActiva 0.2.3_2017.11**
> Es considera el present document com a única referència per a l'elaboració de les proves descrites en aquest test plan.

Introducció
---

El propòsit d'aquest document és descriure el pla de proves a que es sotmetrà les funcionalitats principals del software d'e-commerce Opencart[^opencart] instal·lat al domini [**votarem.lu**](http://opencart.votarem.lu)
[^opencart]: [Opencart](https://www.opencart.com/) OpenCart is an online store management system. It is PHP-based, using a MySQL database and HTML components. Support is provided for numerous languages and currencies, and it is freely available under the GNU General Public License.

Propòsit
---

Es vol comprovar la funcionalitat de la última versió del software des del punt de vista de l'administrador i de l'usuari final

Abast
---

Es comprovarà les següents funcionalitats específiques:
> **Prioritat crítica:**

> - Usuari: Procés de checkout, casos positius i negatius.
> - Administrador: Login, casos positius i negatius.
> - Administrador: Gestió de stocks.
>
> **Proiritat mitjana:**

> - Usuari: resta de funcionalitats.
> - Administrador: resta de funcionalitats.

Criteris
---
Criteris d'aprovació de les proves:

   - En el cas de les proves de proritat crítica el 100% d'aquestes hauran de ser executades satisfactoriament.
   - La resta de proves de prioritat mitjana hauran de ser executades de forma satisfactoria en un percentatge no inferior al 70%.

Tipus de proves
---
Les proves seran automatitzades, amb el framework TestNG[^testng] sobre llenguatge Java, executables des de Maven[^maven].

[^testng]: TestNG is a testing framework for the Java programming language inspired by JUnit and NUnit. The design goal of TestNG is to cover a wider range of test categories: unit, functional, end-to-end, integration, etc.

[^maven]: Apache Maven is a software project management and comprehension tool. Based on the concept of a project object model (POM), Maven can manage a project's build, reporting and documentation from a central piece of information.

Les proves seran d'ambit funcional. Les proves referents a elements que persistiran en properes versions del software passaran aleshores a ser considerades de regresió.

Entorn de proves
---
Es faran proves a entorn desktop/laptop i mobile amb els següents sistemes operatius i navegadors d'internet:

 - **Desktop / Laptop**
    - **Linux**: Chrome + Firefox
    - **MacOS**: Chrome + Firefox
    - **Windows**: Chrome + Firefox
 - **Mobile**
    - **iOS**: Safari
    - **Android**: Chrome

Es proveiran els drivers per executar les proves a entorn físic PC (desktop/laptop) per a Windows i MacOS en cas de disponibilitat. En el cas de no disponibilitat d'un dispositiu Linux les proves s'executaran sobre màquina virtual.

En el cas dels dispositius mòbils es proveirà un accès a servei remot de màquines virtuals de dispositius iPhone i Android.

Calendari de proves
---
La suite de proves es prepararà en el periode que va entre el 30 de novembre i el 14 de desembre de 2017.

Realitzazió de les tasques
---
Les diferents tasques per la realització d'aquest plan quedarà configurat amb la següent disposició:

>  Test Manager :

> - Disseny funcional de les proves.

> Developers i Testers :

> - Desenvolupament de les proves automatitzades.
> - Execució i correcció de les proves.

> Testers i equip de Review :

> - Report sobre els resultats trobats a les proves i redacció d'informe sobre aquestes.