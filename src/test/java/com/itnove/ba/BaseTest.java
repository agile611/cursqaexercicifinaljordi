package com.itnove.ba;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileBrowserType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by jPeralta on 05-12-2017.
 */
public class BaseTest {
    public RemoteWebDriver driver;
    public WebDriverWait wait;
    public Actions actionHover;
    public static String display;
    public static int timeOut;

    @BeforeClass
    public void setUp() throws IOException {
        String os;
        String testdriver = "selenium";
        String device = System.getProperty("device");
        String browser = System.getProperty("browser");
        String driverPath = "src" + File.separator + "main" + File.separator + "resources" + File.separator;
        String chromeDriver = "";
        String firefoxDriver = "";

        os = System.getProperty("os");
        timeOut = 10;
        /* hardcode os: */ os = "linux"; // linux, macos, windows, android, ios

        if (device != null && device.equalsIgnoreCase("iphone"))
            os = "ios";
        if (device != null && device.equalsIgnoreCase("android"))
            os = "android";

        if (os != null && os.equalsIgnoreCase("android")) {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("deviceName", "Android Emulator");
            caps.setCapability("platformVersion", "5.1");
            caps.setCapability("browserName", MobileBrowserType.BROWSER);
            driver = new AndroidDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
            testdriver = "appium";
            display = "mobile";
        } else if (os != null && os.equalsIgnoreCase("ios")) {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability("appiumVersion", "1.7.1");
            caps.setCapability("deviceName", "iPhone 6 Plus Simulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("platformVersion", "10.0");
            caps.setCapability("platformName", "iOS");
            caps.setCapability("browserName", "Safari");
            driver = new IOSDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
            testdriver = "appium";
            display = "mobile";
        } else if (os != null && os.equalsIgnoreCase("macos")) {
            chromeDriver = "chromedriver-macos";
            firefoxDriver = "geckodriver-macos";
            display = "desktop";
        } else if (os != null && os.equalsIgnoreCase("linux")) {
            chromeDriver = "chromedriver-linux";
            firefoxDriver = "geckodriver-linux";
            display = "desktop";
        } else {
            chromeDriver = "chromedriver.exe";
            firefoxDriver = "geckodriver.exe";
            display = "desktop";
        }
        if (!chromeDriver.equals("") || !firefoxDriver.equals("")){
            if (browser != null && browser.equalsIgnoreCase("firefox")) {
                DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                System.setProperty("webdriver.gecko.driver", driverPath+firefoxDriver);
                driver = new FirefoxDriver(capabilities);
            } else {
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                System.setProperty("webdriver.chrome.driver", driverPath+chromeDriver);
                driver = new ChromeDriver(capabilities);
            }
        }
        if (display.equals("mobile"))
            timeOut = 15;


        wait = new WebDriverWait(driver, timeOut);
        actionHover = new Actions(driver);

        driver.manage().deleteAllCookies();
        if (testdriver.equalsIgnoreCase("webdriver"))
            driver.manage().window().fullscreen();
        else
            System.out.println(driver.getSessionId());
        driver.manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

}
