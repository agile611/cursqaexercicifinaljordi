package com.itnove.ba.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static com.itnove.ba.BaseTest.display;

/**
 * Created by guillem on 01/03/16.
 */
public class DashboardPage {

    @FindBy(xpath = "//div[@class='desktop-bar']/ul/li[@id='quickcreatetop']/ul/li[1]/a")
    public WebElement desktopCreateAccountLink;

    @FindBy(xpath = "//div[@class='mobile-bar']/ul/li[@id='quickcreatetop']/ul/li[1]/a")
    public WebElement mobileCreateAccountLink;

    @FindBy(xpath = "//div[@class='desktop-bar']/ul/li[@id='quickcreatetop']/ul/li[5]/a")
    public WebElement desktopCreateDocumentLink;

    @FindBy(xpath = "//div[@class='mobile-bar']/ul/li[@id='quickcreatetop']/ul/li[5]/a")
    public WebElement mobileCreateDocumentLink;

    @FindBy(xpath = "//div[@class='desktop-bar']/ul/li[@id='quickcreatetop']/a")
    public WebElement desktopCreateButton;

    @FindBy(xpath = "//div[@class='mobile-bar']/ul/li[@id='quickcreatetop']/a")
    public WebElement mobileCreateButton;

    @FindBy(xpath = "//*[@id='tab0']/..")
    public WebElement suiteCrmDashboard;

    @FindBy(xpath = ".//*[@id='tab-actions']/a")
    public WebElement actions;

    @FindBy(xpath = ".//*[@id='tab-actions']/ul/li[2]/input")
    public WebElement addTab;

    @FindBy(xpath = ".//*[@id='pagecontent']/div[3]/div/div/div[3]/button[2]")
    public WebElement addTabWidgetButton;

    @FindBy(xpath = "(.//*[@id='usermenucollapsed'])[2]")
    public WebElement icono;

    @FindBy(id = "//div[@class='mobile-bar']/ul/li[@id='globalLinks']")
    public WebElement mobileUserDropDown;

    @FindBy(id = "//div[@class='tablet-bar']/ul/li[@id='globalLinks']")
    public WebElement tabletUserDropDown;

    @FindBy(id = "//div[@class='desktop-bar']/ul/li[@id='globalLinks']")
    public WebElement desktopUserDropDown;

    @FindBy(id = "//div[@class='mobile-bar']/ul/li/ul/li/a[@id='logout_link']")
    public WebElement mobileLogOutUser;

    @FindBy(id = "//div[@class='tablet-bar']/ul/li/ul/li/a[@id='logout_link']")
    public WebElement tabletLogOutUser;

    @FindBy(id = "//div[@class='desktop-bar']/ul/li/ul/li/a[@id='logout_link']")
    public WebElement desktopLogOutUser;

    @FindBy(xpath = "(.//*[@id='searchbutton'])[2]")
    public WebElement lupa;

    @FindBy(xpath = "(.//*[@id='query_string'])[3]")
    public WebElement searchBox;

    @FindBy(xpath = "(.//*[@id='searchformdropdown']/div/span/button)[2]")
    public WebElement iconoLupa;

    public void createButtonClick(Actions hover) throws InterruptedException {
        if (display.equalsIgnoreCase("mobile")) {
            mobileCreateButton.click();
        } else {
            hover.moveToElement(desktopCreateButton)
                .moveToElement(desktopCreateButton)
                .click().build().perform();
        }
    }

    public void clickOnCreateAccountLink(Actions hover, WebDriverWait wait) throws InterruptedException {
        if (display.equalsIgnoreCase("mobile")) {
            mobileCreateButton.click();
            wait.until(ExpectedConditions.visibilityOf(mobileCreateAccountLink));
            mobileCreateAccountLink.click();
        } else {
            hover.moveToElement(desktopCreateButton)
                .moveToElement(desktopCreateAccountLink)
                .click().build().perform();
        }
    }

    public void clickOnCreateDocumentLink(Actions hover, WebDriverWait wait) throws InterruptedException {
        if (display.equalsIgnoreCase("mobile")) {
            mobileCreateButton.click();
            wait.until(ExpectedConditions.visibilityOf(mobileCreateDocumentLink));
            mobileCreateDocumentLink.click();
        } else {
            hover.moveToElement(desktopCreateButton)
                .moveToElement(desktopCreateDocumentLink)
                .click().build().perform();
        }
    }

    public void hoverAndClickEveryCreateLink(WebDriver driver, Actions hover) throws InterruptedException {
        createButtonClick(hover);
        String listElements = "(.//*[@id='quickcreatetop']/ul)[2]/li";
        String lsl = listElements + "/a";
        System.out.println(lsl);
        List<WebElement> listOfCreates = driver.findElements(By.xpath(lsl));
        for (int i = 1; i <= listOfCreates.size(); i++) {
            createButtonClick(hover);
            WebElement eachCreateItem = driver.findElement(By.xpath(listElements + "[" + i + "]/a"));
            hover.moveToElement(desktopCreateButton)
                    .moveToElement(eachCreateItem)
                    .click().build().perform();
        }
    }

    public boolean isDashboardLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(suiteCrmDashboard));
        return suiteCrmDashboard.isDisplayed();
    }

    public void logout(WebDriver driver) {
        driver.navigate().to("http://crm.votarem.lu/index.php?module=Users&action=Logout");
    }

    public void desktopActionAddTab(WebDriverWait wait, Actions hover) throws InterruptedException {
        if (display.equalsIgnoreCase("mobile")){
            actions.click();
            addTab.click();
            wait.until(ExpectedConditions.visibilityOf(addTabWidgetButton));
            addTabWidgetButton.click();
        } else {
        hover.moveToElement(actions)
                .moveToElement(actions)
                .click().build().perform();
        hover.moveToElement(actions)
                .moveToElement(addTab)
                .click().build().perform();
        wait.until(ExpectedConditions.visibilityOf(addTabWidgetButton));
        hover.moveToElement(addTabWidgetButton)
                .moveToElement(addTabWidgetButton)
                .click().build().perform();
        }
    }

    public void search(WebDriver driver, WebDriverWait wait, Actions hover, String keyword) throws InterruptedException {
        hover.moveToElement(lupa)
                .moveToElement(lupa)
                .click().build().perform();
        wait.until(ExpectedConditions.visibilityOf(searchBox));
        searchBox.clear();
        searchBox.sendKeys(keyword);
        hover.moveToElement(iconoLupa)
                .moveToElement(iconoLupa)
                .click().build().perform();
    }

    public DashboardPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
