package com.itnove.ba.crm;

import com.itnove.ba.BaseTest;
import org.testng.annotations.BeforeMethod;

public class BaseTestCRM extends BaseTest{

    @BeforeMethod
    public void loadURL() {
        driver.navigate().to("http://crm.votarem.lu");
    }
}