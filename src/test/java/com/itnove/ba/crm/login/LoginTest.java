package com.itnove.ba.crm.login;

import com.itnove.ba.crm.BaseTestCRM;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginTest extends BaseTestCRM {

    @Test
    public void AppLogIn() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,10);
        LoginPage login = new LoginPage(driver);
        DashboardPage dashboardPage = new DashboardPage(driver);

        // Esperem i comprovem que la opció de login està disponible .
        Assert.assertTrue(login.pageDisplayed(wait));
        // S'introdueix l'usuari correcte, la contrasenya correcta i es fa click al botó de login. 
        login.login("user","bitnami");
        // Comprovem que s'arriba al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
    }

    @Test (dependsOnMethods = "AppLogIn")
    public void AppLogOut() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,10);
        LoginPage login = new LoginPage(driver);
        DashboardPage dashboard = new DashboardPage(driver);

        // Comprovem que es mostra el dashboard
        assertTrue(dashboard.isDashboardLoaded(driver,wait));

        // Redireccionem a la URL de Log Out
        dashboard.logout(driver);

        // Esperem i comprovem que la opció de login està disponible .
        login.pageDisplayed(wait);
        Assert.assertTrue(login.loginButton.isDisplayed());
    }

    @Test (dependsOnMethods = "AppLogOut")
    public void NotValidLogIn() throws InterruptedException {
        //User ok passw KO
        checkErrors("user","nami");
        //User KO passwd OK
        checkErrors("resu","bitnami");
        //User KO passwd KO
        checkErrors("resu","nami");
    }

    private void checkErrors(String user, String passwd){
        // S'introdueix l'usuari, contrasenya i es fa click al botó de login. 
        LoginPage login = new LoginPage(driver);
        login.login(user, passwd);
        // Comprovar l'error
        assertTrue(login.isErrorMessagePresent(wait));
        assertEquals(login.errorMessageDisplayed(),
                "You must specify a valid username and password.");
    }
}
