package com.itnove.ba.crm.search;

import com.itnove.ba.crm.BaseTestCRM;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import com.itnove.ba.crm.pages.SearchResultsPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTest extends BaseTestCRM {

    @Test
    public void searchLogin() throws InterruptedException {
        LoginPage login = new LoginPage(driver);
        DashboardPage dashboardPage = new DashboardPage(driver);

        // Esperem i comprovem que la opció de login està disponible .
        Assert.assertTrue(login.pageDisplayed(wait));

        // S'introdueix l'usuari correcte, contrasenya correcta i es clicka  al botó de login. 
        login.login("user", "bitnami");

        // Comprovem que s'arriba al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));
    }

    @Test (dependsOnMethods = "searchLogin")
    public void resultSearch() throws InterruptedException {
        DashboardPage dashboard = new DashboardPage(driver);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);

        String keyword = "ripoll";

        // Comprovem que s'arriba al dashboard
        assertTrue(dashboard.isDashboardLoaded(driver,wait));

        // Introduïm la paraula de l'element a cercar al camp corresponent i fem click sobre el boto de cerca
        dashboard.search(driver, wait, actionHover, keyword);

        // Comprovem que es mostra la pàgina de resultats
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));

        // Comprovem que es mostra la paraula cercada a la informació de la pàgina
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),keyword);

        // Fem click al primer resultat de la llista de cerca
        searchResultsPage.clickOnFirstResult();

        // Comprovem que el nom de l'element seleccionat coincideix amb l'introduït previament
        assertTrue(driver.getPageSource().toUpperCase().contains(keyword.toUpperCase()));
    }

    @Test (dependsOnMethods = "searchLogin")
    public void noResultSearch() throws InterruptedException {
        DashboardPage dashboard = new DashboardPage(driver);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);

        String keyword = "castanya";
        // Comprovo que arribo al dashboard
        assertTrue(dashboard.isDashboardLoaded(driver,wait));

        // Introduïm la paraula de l'element a cercar al camp corresponent i fem click sobre el boto de cerca
        dashboard.search(driver, wait, actionHover, keyword);

        // Comprovem que es mostra la pàgina de resultats
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));

        // Comprovem que es mostra la paraula cercada a la informació de la pàgina
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),keyword);

        // Comprovem que no es mostra cap element a la pàgina de resultats
        assertTrue(searchResultsPage.isNoSearchResultsDisplayed(wait));
    }
}
