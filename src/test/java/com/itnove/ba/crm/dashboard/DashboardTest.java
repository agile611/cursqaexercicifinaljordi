package com.itnove.ba.crm.dashboard;

import com.itnove.ba.crm.BaseTestCRM;
import com.itnove.ba.crm.pages.*;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.util.UUID;

public class DashboardTest extends BaseTestCRM {

    @Test
    public void dashboardLogin() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        DashboardPage dashboardPage = new DashboardPage(driver);

        // S'introdueix l'usuari correcte, contrasenya correcta i es clicka  al botó de login. 
        loginPage.login("user", "bitnami");

        // Comprovem que s'arriba al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));
    }

    @Test (dependsOnMethods = "deleteAccount")
    public void dashboardHoverClickCreate() throws InterruptedException {
        DashboardPage dashboardPage = new DashboardPage(driver);

        // Comprovem que s'arriba al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));

        // Fem clic sobre tots el links de crear document
        dashboardPage.hoverAndClickEveryCreateLink(driver, actionHover);
    }


    private String accountName = UUID.randomUUID().toString();

    @Test (dependsOnMethods = "dashboardLogin")
    public void createAccount() throws InterruptedException {
        DashboardPage dashboardPage = new DashboardPage(driver);
        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        EditAccountPage editAccountPage = new EditAccountPage(driver);

        // Comprovem que s'arriba al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));

        // Fem click al dropdown de crear element
        dashboardPage.createButtonClick(actionHover);

        // Fem click al botó de crear compte
        dashboardPage.clickOnCreateAccountLink(actionHover, wait);

        // Introduïm un nom per al compte
        createAccountPage.fillName(accountName);

        // Fem click a desar el compte
        createAccountPage.saveAccount();

        // Comprovem que el nom del compte correspon amb l'introduït
        assertEquals(accountName.toUpperCase(),editAccountPage.getTitulo().toUpperCase());
    }

    @Test (dependsOnMethods = "createAccount")
    public void deleteAccount() throws InterruptedException {
        DashboardPage dashboardPage = new DashboardPage(driver);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        EditAccountPage editAccountPage = new EditAccountPage(driver);

        // Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));

        // Introduïm, al camp de cerca,el nom de compte creat al mètode anterior
        dashboardPage.search(driver, wait, actionHover, accountName);

        // Esperem i compreovem que es mostri el resultat de la cerca
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));

        // Comprovem que el resultat de la cerca coincideixi amb el del compte creat
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),accountName);

        // Fem click al resultat mostrat (al primer si n'hi ha més d'un) per borrar-ho
        searchResultsPage.clickOnFirstResult();

        // Comprovem que a la pàgina d'esborrar compte existeix el nom del compte creat
        assertTrue(driver.getPageSource().toUpperCase().contains(accountName.toUpperCase()));

        // Comprovem que a la pàgina d'esborrar compte el nom coincideix amb l'esperat
        assertEquals(accountName.toUpperCase(),editAccountPage.getTitulo().toUpperCase());

        // Fem click al botó de eliminar compte
        editAccountPage.deleteAccount(driver, actionHover);

        // Anotem el nom del compte manipulat a consola
        System.out.println(accountName);
    }

    @Test (dependsOnMethods = "dashboardLogin")
    public void addDashboardTab() throws InterruptedException {
        DashboardPage dashboardPage = new DashboardPage(driver);

        // Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));

        // Afegim un tab al dashboard
        dashboardPage.desktopActionAddTab(wait, actionHover);
    }

    @Test (dependsOnMethods = "dashboardLogin")
    public void createDocument() throws InterruptedException {
        DashboardPage dashboardPage = new DashboardPage(driver);
        CreateDocumentPage createDocumentPage = new CreateDocumentPage(driver);

        String name = UUID.randomUUID().toString();
        //Comprovem que s'arriba al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));

        // Fem click al dropdown de crear element
        dashboardPage.createButtonClick(actionHover);

        // Fem click al botó de crear document
        dashboardPage.clickOnCreateDocumentLink(actionHover, wait);

        // Instanciem l'element a pujar
        File file = new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "2-logo-B_activa.png");

        // Introduïm la localització del fitxer al camp del formulari
        createDocumentPage.browseFile(file.getAbsolutePath());

        // Fem click al botó "Save"
        createDocumentPage.saveDocument();
    }
}
